<?php

use App\Core\Request;
?>
<!doctype html>
<html lang="zxx">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="<?= ASSETS; ?>main/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/animate.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/fontawesome.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/flaticon.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/magnific-popup.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/nice-select.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/slick.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/meanmenu.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/odometer.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/style.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/responsive.css">
<title><?= APP_NAME; ?></title>
<link rel="icon" type="image/png" href="<?= ASSETS; ?>main/img/favicon.png">
</head>

<div class="preloader">
<div class="loader">
<div class="shadow"></div>
<div class="box"></div>
</div>
</div>


<div class="navbar-area">
<div class="luvion-responsive-nav">
<div class="container">
<div class="luvion-responsive-menu">
<div class="logo">
<a href="<?= APP_URL; ?>">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="logo">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="logo">
</a>
</div>
</div>
</div>
</div>
<div class="luvion-nav">
<div class="container">
<nav class="navbar navbar-expand-md navbar-light">
<a class="navbar-brand" href="<?= APP_URL; ?>">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="logo" width="150">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="logo" width="150">
</a>
<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
<ul class="navbar-nav">

<li class="nav-item">
    <a href="<?= HOME; ?>" class="nav-link <?= Request::path() === HOME ? 'active' : ''; ?>">Home </a>
</li>

<li class="nav-item">
    <a href="<?= ABOUT; ?>" class="nav-link <?= Request::path() === ABOUT ? 'active' : ''; ?>">About Us</a>
</li>

<li class="nav-item">
    <a href="<?= SERVICES; ?>" class="nav-link <?= Request::path() === SERVICES ? 'active' : ''; ?>">Services</a>
</li>

<li class="nav-item">
    <a href="/contact" class="nav-link <?= Request::path() === CONTACT ? 'active' : ''; ?>">Contact</a>
</li>
</ul>
<div class="others-options">
<?php if (isset($_SESSION['login'])) : ?>
    <a href="<?= DASHBOARD; ?>" class="login-btn"><i class="flaticon-user"></i> Dashboard </a>
<?php else : ?>
    <a href="<?= LOGIN; ?>" class="login-btn"><i class="flaticon-user"></i> Log In</a>
<?php endif; ?>
</div>
</div>
</nav>
</div>
</div>
</div>

{{content}}

<section class="account-create-area">
<div class="container">
<div class="account-create-content">
<h2>Apply for an account in minutes</h2>
<p>Get your Summerville account today!</p>
<a href="<?= REGISTER; ?>" class="btn btn-primary">Get Your summerville Account</a>
</div>
</div>
</section>


<footer class="footer-area">
<div class="container">
<div class="row">
<div class="col-lg-3 col-sm-6 col-md-6">
<div class="single-footer-widget">
<div class="logo">
<a href="<?= APP_URL; ?>"><img src="<?= ASSETS; ?>main/img/black-logo.png" alt="logo"></a>
<p>We've made it easy for you to do your banking 24 hours a day, 7 days a week, from your living room couch, a coffee shop, out in the fields, or elsewhere you can access the internet.</p>
</div>
<ul class="social-links">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-md-6">
<div class="single-footer-widget">
<h3>Company</h3>
<ul class="list">
<li><a href="<?= ABOUT; ?>">About Us</a></li>
<li><a href="<?= SERVICES; ?>">Services</a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-md-6">
<div class="single-footer-widget">
<h3>Support</h3>
<ul class="list">
<li><a href="<?= CONTACT; ?>">Contact Us</a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-md-6">
<div class="single-footer-widget">
<h3>Address</h3>
<ul class="footer-contact-info">
<li><span>Location:</span> 27 Division St, New York, NY 10002, USA</li>
<li><span>Email:</span> <a href="#"><span class="__cf_email__" >hello@summervillecreditunion.com</span></a></li>
<li><span>Phone:</span> <a href="tel:+321984754">+ (321) 984 754</a></li>
<li><span>Fax:</span> <a href="tel:+12129876543">+1-212-9876543</a></li>
</ul>
</div>
</div>
</div>
<div class="copyright-area">
<p>Copyright © 2021 | <?= APP_NAME; ?></p>
</div>
</div>
<div class="map-image"><img src="<?= ASSETS; ?>main/img/map.png" alt="map"></div>
</footer>

<div class="go-top"><i class="fas fa-arrow-up"></i></div>

<script data-cfasync="false" src="<?= ASSETS; ?>main/js/email-decode.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.min.js"></script>
<script src="<?= ASSETS; ?>main/js/popper.min.js"></script>
<script src="<?= ASSETS; ?>main/js/bootstrap.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.meanmenu.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.nice-select.min.js"></script>
<script src="<?= ASSETS; ?>main/js/slick.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.magnific-popup.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.appear.min.js"></script>
<script src="<?= ASSETS; ?>main/js/odometer.min.js"></script>
<script src="<?= ASSETS; ?>main/js/owl.carousel.min.js"></script>
<script src="<?= ASSETS; ?>main/js/parallax.min.js"></script>
<script src="<?= ASSETS; ?>main/js/wow.min.js"></script>
<script src="<?= ASSETS; ?>main/js/form-validator.min.js"></script>
<script src="<?= ASSETS; ?>main/js/contact-form-script.js"></script>
<script src="<?= ASSETS; ?>main/js/main.js"></script>
</body>


</html>