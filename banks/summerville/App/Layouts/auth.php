
<!doctype html>
<html lang="zxx">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="<?= ASSETS; ?>main/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/animate.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/fontawesome.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/flaticon.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/magnific-popup.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/nice-select.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/slick.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/meanmenu.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/odometer.min.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/style.css">
<link rel="stylesheet" href="<?= ASSETS; ?>main/css/responsive.css">
<title><?= APP_NAME; ?></title>
<link rel="icon" type="image/png" href="<?= ASSETS; ?>main/img/favicon.png">
<script src="<?= ASSETS; ?>main/js/jquery.min.js"></script>
</head>

<div class="preloader">
<div class="loader">
<div class="shadow"></div>
<div class="box"></div>
</div>
</div>

{{content}}


<script src="<?= ASSETS; ?>main/js/jquery.min.js"></script>
<script src="<?= ASSETS; ?>main/js/popper.min.js"></script>
<script src="<?= ASSETS; ?>main/js/bootstrap.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.meanmenu.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.nice-select.min.js"></script>
<script src="<?= ASSETS; ?>main/js/slick.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.magnific-popup.min.js"></script>
<script src="<?= ASSETS; ?>main/js/jquery.appear.min.js"></script>
<script src="<?= ASSETS; ?>main/js/odometer.min.js"></script>
<script src="<?= ASSETS; ?>main/js/owl.carousel.min.js"></script>
<script src="<?= ASSETS; ?>main/js/parallax.min.js"></script>
<script src="<?= ASSETS; ?>main/js/wow.min.js"></script>
<script src="<?= ASSETS; ?>main/js/form-validator.min.js"></script>
<script src="<?= ASSETS; ?>main/js/contact-form-script.js"></script>
<script src="<?= ASSETS; ?>main/js/main.js"></script>
</body>

<!-- Mirrored from templates.envytheme.com/luvion/default/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Mar 2021 08:31:02 GMT -->
</html>