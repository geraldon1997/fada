<?php

use App\Models\User;
use App\Controllers\UserController as UC;
use App\Controllers\ProfileController;

$profile = new ProfileController;
$user = new UC;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?= APP_NAME; ?></title>
    <!-- Favicon icon -->
    <link rel="icon" href="<?= ASSETS; ?>main/images/nestcity.png" type="image/x-icon" />
    <link href="<?= ASSETS; ?>dashboard/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= ASSETS; ?>dashboard/vendor/chartist/css/chartist.min.css">
    <!-- Vectormap -->
    <link href="<?= ASSETS; ?>dashboard/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="<?= ASSETS; ?>dashboard/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?= ASSETS; ?>dashboard/css/style.css" rel="stylesheet">
    <link href="<?= ASSETS; ?>dashboard/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <style>
        .deznav{
            margin-top: -100px;
        }
        .nav-header{
            margin-left: 20px;
            width: 200px;
        }
        .header{
            height: 50px;
        }
        .nav-header .logo-abbr{
            max-width: 100px;
            margin-top: -70px;
        }
        
        @media (max-width: 720px) {
            .nav-header{
                margin-left: 5px;
                width: auto;
                padding-top: 0;
                padding-bottom: 0;
                height: 30px;
                margin-top: 20px;
            }
            .nav-header .logo-abbr{
                margin-top: -25px;
            }
            .nav-control{
                margin-top: -15px;
            }
            a .brand-logo{
                height: 50px !important;
            }
        }
        
    </style>
</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="/dashboard" class="brand-logo" >
                <img class="logo-abbr" src="<?= ASSETS; ?>main/images/nestcity.png" alt="<?= APP_NAME; ?>" width="150">
                <img class="logo-compact" src="<?= ASSETS; ?>main/images/nestcity.png" alt="<?= APP_NAME; ?>" width="150">
                <img class="brand-title" src="<?= ASSETS; ?>main/images/nestcity.png" alt="<?= APP_NAME; ?>" width="150">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                    
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
        
        
        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="dashboard_bar">
                                
                            </div>
                        </div>
                        <ul class="navbar-nav header-right">
                            
                            
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <div class="header-info">
                                        <span class="text-black">Hello, <strong><?= User::isLoggedIn() ? $_SESSION['login'] : 'user'; ?></strong></span>
                                        <p class="fs-12 mb-0">Welcome back</p>
                                    </div>
                                    <?php if ($profile->data()[0]['id_url']) : ?>
                                        <img src="<?= $profile->data()[0]['id_url']; ?>" width="20" alt=""/>
                                    <?php else : ?>
                                        <img src="<?= ASSETS; ?>dashboard/images/avatar/1.png" alt="">
                                    <?php endif; ?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/profile" class="dropdown-item ai-icon">
                                        <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    
                                    <a href="#" id="logout" class="dropdown-item ai-icon">
                                        <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php if ($user->hasProfile() || $_SESSION['login'] === 'admin') : ?>
        <div class="deznav">
            <div class="deznav-scroll">
                <ul class="metismenu" id="menu">
                    <li title="Home">
                        <a class="has-arrow ai-icon" href="<?= HOME; ?>" aria-expanded="false">
                            <i class="flaticon-381-home"></i>
                            <span class="nav-text">Home</span>
                        </a>
                    </li>
                    <li title="Dashboard">
                        <a class="has-arrow ai-icon" href="<?= DASHBOARD; ?>" aria-expanded="false">
                            <i class="flaticon-381-networking"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <?php if ($_SESSION['login'] === 'admin') : ?>
                    <li title="Users">
                        <a class="has-arrow ai-icon" href="<?= VIEW_USERS; ?>" aria-expanded="false">
                            <i class="flaticon-381-user"></i>
                            <span class="nav-text">Users</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    
                    <?php if ($_SESSION['login'] !== 'admin') : ?>
                    <li title="Transfer">
                        <a class="has-arrow ai-icon" href="<?= TRANSFER; ?>" aria-expanded="false">
                            <i class="flaticon-381-controls-3"></i>
                            <span class="nav-text">Transfer</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    
                    <li title="History">
                        <a class="has-arrow ai-icon" href="<?= HISTORY; ?>" aria-expanded="false">
                            <i class="flaticon-381-internet"></i>
                            <span class="nav-text">History</span>
                        </a>
                    </li>
                    <li title="Settings">
                        <a class="has-arrow ai-icon" href="<?= SETTINGS; ?>" aria-expanded="false">
                            <i class="flaticon-381-settings-2"></i>
                            <span class="nav-text">Account Settings</span>
                        </a>
                    </li>
                </ul>
                
                <div class="copyright">
                    <p><strong><?= APP_NAME; ?></strong> © 2021 All Rights Reserved</p>
                    
                </div>
            </div>
        </div>
        <?php endif; ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
        
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                {{content}}
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © <a href="<?= APP_URL; ?>"><?= APP_NAME; ?></a> | All Rights Reserved <?= date('Y'); ?></p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script>
        $('a#logout').click(() => {
            $.ajax({
                type: 'POST',
                url: "<?= LOGOUT; ?>",
                data: {status:1}
            }).done(result => {
                return location = result;
            })
        })
    </script>
    <!-- Required vendors -->
    <script src="<?= ASSETS; ?>dashboard/vendor/global/global.min.js"></script>
    <script src="<?= ASSETS; ?>dashboard/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?= ASSETS; ?>dashboard/vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="<?= ASSETS; ?>dashboard/js/custom.min.js"></script>
    <script src="<?= ASSETS; ?>dashboard/js/deznav-init.js"></script>
    <script src="<?= ASSETS; ?>dashboard/vendor/owl-carousel/owl.carousel.js"></script>     
    
    <!-- Chart piety plugin files -->
    <script src="<?= ASSETS; ?>dashboard/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Apex Chart -->
    <script src="<?= ASSETS; ?>dashboard/vendor/apexchart/apexchart.js"></script>
    
    <!-- Dashboard 1 -->
    <script src="<?= ASSETS; ?>dashboard/js/dashboard/dashboard-1.js"></script>
    

    <script>
        function carouselReview(){
            /*  testimonial one function by = owl.carousel.js */
            /*  testimonial one function by = owl.carousel.js */
            jQuery('.testimonial-one').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                center:true,
                dots: false,
                navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
                responsive:{
                    0:{
                        items:2
                    },
                    400:{
                        items:3
                    },  
                    700:{
                        items:5
                    },  
                    991:{
                        items:6
                    },          
                    
                    1200:{
                        items:4
                    },
                    1600:{
                        items:5
                    }
                }
            })  
        }
        
        jQuery(window).on('load',function(){
            setTimeout(function(){
                carouselReview();
            }, 1000); 
        });
    </script>
</body>


</html>