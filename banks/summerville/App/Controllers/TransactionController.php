<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Response;
use App\Models\Transaction;

use function App\Core\view;

class TransactionController extends Controller
{
    public function history()
    {
        $account = new AccountController;
        $history = $this->myTransactions($account->id());
        return view('dashboard/history', $history);
    }

    public function myTransactions(string $accountid)
    {
        return Transaction::where(['account_id' => $accountid]);
    }

    public function addTransaction()
    {
        $account = new AccountController;
        $data = [
            'account_id' => $account->id(),
            'bank' => $_SESSION['bank'],
            'baddr' => $_SESSION['baddr'],
            'routing' => $_SESSION['route'],
            'recipient' => $_SESSION['rname'],
            'acctnum' => $_SESSION['acctnum'],
            'transaction_amount' => $_SESSION['amount'],
            'date_of_transfer' => $_SESSION['dot'],
            'transfer_type' => $_SESSION['transfer'],
            'transaction_type' => 'debit',
            'purpose' => $_SESSION['purpose'],
            'date' => date('Y-m-d')
        ];
        Transaction::create($data);

        $mail = new Mail;
        $user = new UserController;
        $profile = new ProfileController;
        $home = new HomeController;

        $name = $profile->data()[0]['firstname'].' '.$profile->data()[0]['lastname'];
        
        $body = $home->mailHeader($name);
        $body .= "<h4>This alert is generated to notify you of recent activity on your account.</h4>";
        $body .= "<h3><i>The details about this transaction are as follows:</i></h3>";
        $body .= "<table border=1>";
        $body .= "<tr><td>Bank</td> <td>".$_SESSION['bank']."</td></tr>";
        $body .= "<tr><td>Account Number</td> <td>".$_SESSION['acctnum']."</td></tr>";
        $body .= "<tr><td>transfer type</td> <td>".$_SESSION['transfer']."</td></tr>";
        $body .= "<tr><td>Amount</td> <td>".$_SESSION['amount']."</td></tr>";
        $body .= "<tr><td>Purpose</td> <td>".$_SESSION['purpose']."</td></tr>";
        $body .= "</table>";
        $body .= $home->mailFooter();

        $mail->sendMail($user->email(), "Debit Alert", $body);

        unset($_SESSION['bank']);
        unset($_SESSION['baddr']);
        unset($_SESSION['route']);
        unset($_SESSION['rname']);
        unset($_SESSION['acctnum']);
        unset($_SESSION['amount']);
        unset($_SESSION['dot']);
        unset($_SESSION['transfer']);
        unset($_SESSION['purpose']);

        return 'success';
    }

    public function credit($data)
    {
        return Transaction::create($data);
    }

    public function trn()
    {
        $account = new AccountController;
        $data = [
            'account_id' => $account->id(),
            'bank_name' => 'first bank',
            'account_number' => '1234567890',
            'routing' => '12345',
            'transaction_type' => 'debit',
            'transaction_amount' => '200',
            'date' => date('Y-m-d')
        ];
        return Transaction::create($data);
    }

    public function showAll()
    {
        return Transaction::allDesc();
    }
}
