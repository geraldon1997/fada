<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Response;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function addProfile()
    {
        $user = new UserController;

        $id = $this->uploadID();

        $data = [
            'user_id' => $user->id(),
            'lastname' => $_POST['ln'],
            'firstname' => $_POST['fn'],
            'gender' => $_POST['gender'],
            'dob' => $_POST['dob'],
            'marital_status' => $_POST['ms'],
            'country_of_origin' => $_POST['co'],
            'state_of_origin' => $_POST['so'],
            'phone' => $_POST['phone'],
            'residence' => $_POST['res'],
            'state_of_residence' => $_POST['sr'],
            'country_of_residence' => $_POST['cr'],
            'occupation' => $_POST['occu'],
            'id_url' => $id
        ];
        
        Profile::create($data);
        // $account = new AccountController;
        // $account->addAccount();
        return Response::redirect(DASHBOARD);
    }

    public function uploadID()
    {
        // get details of the uploaded file
        $fileTmpPath = $_FILES['id']['tmp_name'];
        $fileName = $_FILES['id']['name'];
        $fileSize = $_FILES['id']['size'];
        $fileType = $_FILES['id']['type'];

        if (empty($fileTmpPath)) {
            return 'none';
        }

        // file directory to upload
        $uploadDir = 'App/uploads/';
        $file = $uploadDir.$fileName;

        if (move_uploaded_file($fileTmpPath, $file)) {
            return $file;
        } else {
            return 'error';
        }
    }

    public function data()
    {
        $user = new UserController;
        return Profile::where(['user_id' => $user->id()]);
    }

    public function updateProfile()
    {
        $user = new UserController;
        
        $id = $this->uploadID();

        if ($id === 'none') {
            $data = [
                'lastname' => $_POST['ln'],
                'firstname' => $_POST['fn'],
                'gender' => $_POST['gender'],
                'dob' => $_POST['dob'],
                'marital_status' => $_POST['ms'],
                'country_of_origin' => $_POST['co'],
                'state_of_origin' => $_POST['so'],
                'phone' => $_POST['phone'],
                'residence' => $_POST['res'],
                'state_of_residence' => $_POST['sr'],
                'country_of_residence' => $_POST['cr'],
                'occupation' => $_POST['occu'],
                'user_id' => $user->id()
            ];
            Profile::update($data);
            return Response::redirect(DASHBOARD);
        }
        
        $data = [
            'lastname' => $_POST['ln'],
            'firstname' => $_POST['fn'],
            'gender' => $_POST['gender'],
            'dob' => $_POST['dob'],
            'marital_status' => $_POST['ms'],
            'country_of_origin' => $_POST['co'],
            'state_of_origin' => $_POST['so'],
            'phone' => $_POST['phone'],
            'residence' => $_POST['res'],
            'state_of_residence' => $_POST['sr'],
            'country_of_residence' => $_POST['cr'],
            'occupation' => $_POST['occu'],
            'id_url' => $id,
            'user_id' => $user->id()
        ];
        Profile::update($data);
        return Response::redirect(DASHBOARD);
    }

    public function user($id)
    {
        return Profile::where(['user_id' => $id]);
    }
}
