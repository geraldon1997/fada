<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Response;
use App\Models\User;
use App\Controllers\Mail;

use function App\Core\view;

class UserController extends Controller
{
    public function createAccountForm()
    {
        return view('auth/register');
    }

    public function login()
    {
        return view('auth/login');
    }

    public function forgotPasswordForm()
    {
        return view('auth/forgotpassword');
    }

    public function sendPasswordResetLink()
    {
        $email = $_POST['email'];
        $user = User::where(['email' => $email]);
        if (count($user) < 1) {
            return 'Invalid Email Address';
        }
        $mail = new Mail;
        $home = new HomeController;
        $expire = time() + 3600 ;

        $body = $home->mailHeader($user[0]['login_id']);
        $body .= "Please click on the button below to reset password <br> <a style='color:white; background:blue; border-radius:5px; text-decoration:none; padding:5px;' href='".rtrim(APP_URL, '/').RESET_PASSWORD."/$email#$expire'>Reset Password</a>";
        $body .= "<hr>";
        $body .= $home->mailFooter();

        $mail->sendMail($email, 'Password Reset', $body);
        return 'A Password Reset Link has been sent to your email';
    }

    public function resetPassword()
    {
        return view('auth/resetpassword');
    }

    public function setNewPassword()
    {
        $data = [
            'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
            'email' => $_POST['email']
        ];
        User::update($data);
        return 'success';
    }

    public function dashboard()
    {
        $account = new AccountController;
        $name = new ProfileController;
        return view('dashboard/index', ['account' => $account->myAccount()[0], 'name' => $name->data()[0]]);
    }

    public function profile()
    {
        $profile = new ProfileController;
        return view('dashboard/profile', $profile->data());
    }

    public function settings()
    {
        return view('dashboard/settings');
    }

    public function addUser()
    {
        $email = $_POST['email'];
        $login_id = explode('@', $email)[0].rand(111111, 999999);
        $password = rand(111111, 999999);

        $data = [
            'email' => $email,
            'login_id' => $login_id,
            'password' => password_hash($password, PASSWORD_BCRYPT),
            'is_banned' => 0
        ];
        // return var_dump($data);
        $create = User::create($data);
        
        if ($create) {
            $uid = User::where(['email' => $email])[0]['id'];

            $auth = new AuthController;
            $auth->addAuth(['user_id' => $uid, 'pin' => rand(1111, 9999)]);

            $account = new AccountController;
            $account->addAccount($uid);

            $account_num = $account->accountNumber($uid);
            $transfer_pin = $auth->transferPin($uid);

            $this->sendLoginDetails($email, $login_id, $password, $account_num, $transfer_pin);
            return 'success';
        } else {
            return 'error';
        }
    }

    public function sendLoginDetails($email, $login_id, $password, $account_num, $transfer_pin)
    {
        $home = new HomeController;

        $body = $home->mailHeader($login_id);
        $body .= "Welcome to ". APP_NAME. ", Below are your Account details. <br>";
        $body .= " Email: $email <br> Login ID: $login_id <br> Password: $password <br> Account Number: $account_num <br> Transfer Pin: $transfer_pin";
        $body .= $home->mailFooter();
        return Mail::sendMail($email, "Welcome to ". APP_NAME ."", $body);
    }

    public function authUser()
    {
        
        $login_id = $_POST['login_id'];
        $password = $_POST['password'];
        
        $user_exists = User::where(['login_id' => $login_id]);
        
        if ($user_exists) {
            $verify = password_verify($password, $user_exists[0]['password']);
            if ($verify) {
                $is_banned = $user_exists[0]['is_banned'];

                if ($is_banned) {
                    return "$login_id your account is restricted, \n please contact Support";
                }
                $_SESSION['login'] = $login_id;
                return 'success';
            }
            return 'Invalid login ID or Password';
        } else {
            return 'OOPs! you do not have an account with us';
        }
    }

    public function logoutUser()
    {
        if ($_POST['status'] == 1) {
            unset($_SESSION['login']);
            return HOME;
        }
    }

    public function id()
    {
        return User::where(['login_id' => $_SESSION['login']])[0]['id'];
    }

    public function email()
    {
        return User::where(['login_id' => $_SESSION['login']])[0]['email'];
    }

    public function updatePassword()
    {
        $user = new UserController;

        $data = [
            'password' => password_hash($_POST['np'], PASSWORD_BCRYPT),
            'user_id' => $user->id()
        ];
        User::update($data);
        return Response::redirect(GO_BACK);
    }

    public function hasProfile()
    {
        $profile = new ProfileController;
        if (count($profile->data()) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function showAll()
    {
        $users = User::all();
        return view('dashboard/users', $users);
    }

    public function show($id)
    {
        $profile = new ProfileController;
        return view('dashboard/user', $profile->user($id));
    }

    public function ban($id)
    {
        User::update(['is_banned' => 1, 'id' => $id]);
        return Response::redirect(GO_BACK);
    }

    public function unban($id)
    {
        User::update(['is_banned' => 0, 'id' => $id]);
        return Response::redirect(GO_BACK);
    }

    public function delete($id)
    {
        User::delete(['id' => $id]);
        return Response::redirect(GO_BACK);
    }

    public function getUsername($userid)
    {
        return User::where(['id' => $userid])[0]['login_id'];
    }
}
