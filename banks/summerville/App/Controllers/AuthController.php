<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Response;
use App\Models\Auth;

use function App\Core\view;

class AuthController extends Controller
{
    public function addAuth($auths)
    {
        return Auth::create($auths);
    }

    public function addToken($token)
    {
        return Auth::update($token);
    }

    public function transferPin($userid)
    {
        return Auth::where(['user_id' => $userid])[0]['pin'];
    }

    public function checkpin()
    {
        $pin = $_POST['pincode'];
        
        $mainpin = Auth::where(['pin' => $pin]);
        
        if (empty($mainpin)) {
            return 'error_pin';
        }

        $account = new AccountController;
        $user = new UserController;

        if ($account->isLocked($user->id())) {
            return 'error_account_locked';
        }
        // return json_encode($_POST);

        $bank = $_POST['bank'];
        $baddr = $_POST['baddr'];
        $route = $_POST['route'];
        $rname = $_POST['rname'];
        $acctnum = $_POST['acctnum'];
        $amount = $_POST['amount'];
        $dot = $_POST['dot'];
        $transfer = $_POST['transfer'];
        $purpose = $_POST['purpose'];
        
        if ($this->checkBalance($amount) === 'low') {
            return 'error_balance_low';
        } elseif ($this->checkBalance($amount) === 'high') {
            return 'error_balance_high';
        } elseif ($this->checkBalance($amount) === 'good') {
            $token = rand(111111, 999999);
            $user = new UserController;
            $this->addToken(['token' => $token, 'user_id' => $user->id()]);
            $this->sendTokenToEmail($user->email(), $token, $amount);
            
            $_SESSION['bank'] = $bank;
            $_SESSION['baddr'] = $baddr;
            $_SESSION['route'] = $route;
            $_SESSION['rname'] = $rname;
            $_SESSION['acctnum'] = $acctnum;
            $_SESSION['amount'] = $amount;
            $_SESSION['dot'] = $dot;
            $_SESSION['transfer'] = $transfer;
            $_SESSION['purpose'] = $purpose;

            return 'success';
        }
    }

    public function checkBalance($amount)
    {
        $account = new AccountController;
        
        if ($amount < 100) {
            return 'low';
        } elseif ($amount > $account->balance()) {
            return 'high';
        } else {
            return 'good';
        }
    }

    public function sendTokenToEmail($to, $token, $amount)
    {
        $home = new HomeController;
        $profile = new ProfileController;
        $name = $profile->data()[0]['lastname'].' '.$profile->data()[0]['firstname'];
        $body = $home->mailHeader($name);
        $body .= "<p>please use the token below to complete your transfer of $$amount</p><br><b>$token</b>";
        $body .= "<hr>";
        $body .= $home->mailFooter();
        Mail::sendMail($to, "TRANSFER TOKEN", $body);
    }

    public function resendTokenToEmail()
    {
        $home = new HomeController;
        $user = new UserController;
        $profile = new ProfileController;
        $name = $profile->data()[0]['lastname'].' '.$profile->data()[0]['firstname'];

        $amount = $_SESSION['amount'];
        $to =  $user->email();

        $body = $home->mailHeader($name);
        $body .= "<p>please use the token below to complete your transfer of $$amount</p><br><b>".$this->token()."</b>";
        $body .= "<hr>";
        $body .= $home->mailFooter();
        Mail::sendMail($to, "TRANSFER TOKEN", $body);
        return 'Token has been resent to your email';
    }

    public function showform()
    {
        return view('dashboard/verify_token');
    }

    public function token()
    {
        $user = new UserController;
        return Auth::where(['user_id' => $user->id()])[0]['token'];
    }

    public function checktransfertoken()
    {
        $token = $_POST['token'];
        if ($token !== $this->token()) {
            return 'error';
        } else {
            $account = new AccountController;
            $account->completeTransfer();
            return 'success';
        }
    }

    public function updatePin()
    {
        $user = new UserController;

        $data = [
            'pin' => $_POST['ntp'],
            'user_id' => $user->id()
        ];

        Auth::update($data);
        return Response::redirect(GO_BACK);
    }
}
