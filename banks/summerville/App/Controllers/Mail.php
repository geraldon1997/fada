<?php
namespace App\Controllers;

class Mail
{
    public static function sendMail($toEmail, $subjectEmail, $body)
    {
        $to = $toEmail;
        $subject = $subjectEmail;
        $from = 'support@nestcitybank.com';
 
// To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
        $headers .= 'From: '.APP_NAME.' <'.$from.">\r\n".
        'Reply-To: '.$from."\r\n" .
        'X-Mailer: PHP/' . phpversion();
 
// Compose a simple HTML email message
        $message = "<html><body>";
        $message .= "<p style='color:#080;font-size:18px;'>$body</p>";
        $message .= "</body></html>";

        return mail($to, $subject, $message, $headers);
    }
}
