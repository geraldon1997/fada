<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Response;
use App\Models\Account;

use function App\Core\view;

class AccountController extends Controller
{
    public function addAccount($userid)
    {
        $user = new UserController;

        $data = [
            'user_id' => $userid,
            'account_type' => 'savings',
            'account_number' => '233'.rand(1111111, 9999999),
            'account_balance' => 0,
            'date' => date('Y-m-d')
        ];
        return Account::create($data);
    }

    public function accountNumber($userid)
    {
        return Account::where(['user_id' => $userid])[0]['account_number'];
    }

    public function transfer()
    {
        return view('dashboard/transfer');
    }

    public function myAccount()
    {
        $user = new UserController;
        $userid = $user->id();
        return Account::where(['user_id' => $userid]);
    }

    public function balance()
    {
        $user = new UserController;
        $userid = $user->id();
        return Account::where(['user_id' => $userid])[0]['account_balance'];
    }

    public function id()
    {
        $user = new UserController;
        $userid = $user->id();
        return Account::where(['user_id' => $userid])[0]['id'];
    }

    public function history()
    {
        $history = new TransactionController;
        if ($_SESSION['login'] === 'admin') {
            $transactions = $history->showAll();
        } else {
            $transactions = $history->myTransactions($this->id());
        }
        return view('dashboard/history', $transactions);
    }

    public function completeTransfer()
    {
        $new_balance = $this->balance() - $_SESSION['amount'];
        $data = [
            'account_balance' => $new_balance,
            'id' => $this->id()
        ];
        Account::update($data);
        $transaction = new TransactionController;
        $transaction->addTransaction();
    }

    public function isLocked($id)
    {
        return Account::where(['user_id' => $id])[0]['is_locked'];
    }

    public function lock($id)
    {
        Account::update(['is_locked' => true, 'user_id' => $id]);
        return Response::redirect(GO_BACK);
    }

    public function unlock($id)
    {
        Account::update(['is_locked' => false, 'user_id' => $id]);
        return Response::redirect(GO_BACK);
    }

    public function showCreditForm($id)
    {
        return view('dashboard/credit_form', [$id]);
    }

    public function getID($userid)
    {
        return Account::where(['user_id' => $userid])[0]['id'];
    }

    public function creditUser()
    {
        $accountid = $this->getID($_POST['userid']);
        $old_balance = Account::where(['user_id' => $_POST['userid']])[0]['account_balance'];
        $new_balance = $old_balance + $_POST['amount'];
        $data = [
            'account_balance' => $new_balance,
            'id' => $accountid
        ];
        
        Account::update($data);
        $trans = [
            'account_id' => $accountid,
            'bank' => $_POST['bank'],
            'baddr' => $_POST['baddr'],
            'routing' => $_POST['route'],
            'recipient' => $_POST['rn'],
            'acctnum' => $_POST['an'],
            'transaction_amount' => $_POST['amount'],
            'date_of_transfer' => $_POST['dot'],
            'transfer_type' => $_POST['transfer'],
            'transaction_type' => 'credit',
            'purpose' => $_POST['purpose'],
            'date' => date('Y-m-d')
        ];
        
        $transaction = new TransactionController;
        $transaction->credit($trans);
        return Response::redirect(HISTORY);
        // return var_dump($_POST);
    }

    public function getUserId($accountid)
    {
        return Account::where(['id' => $accountid])[0]['user_id'];
    }
}
