<?php
namespace App\Controllers;

use App\Core\Controller;

use function App\Core\view;

class HomeController extends Controller
{
    public function home()
    {
        return view('main/home');
    }

    public function about()
    {
        return view('main/about');
    }

    public function contact()
    {
        return view('main/contact');
    }

    public function services()
    {
        return view('main/services');
    }

    public function faqs()
    {
        return view('main/faqs');
    }

    public function mailFooter()
    {
        $body = "<hr>";
        $body .= "<div style='background: black; color: white; padding: 10px;'>";
        $body .= "<p style='color: red;'><i>Security Alert: ".APP_NAME." will never request for your card number, PIN or Internet Banking password either on phone or online.<br>Please do not respond to any such requests.</i></p>";
        $body .= "<p>Thank you for choosing Nestcitybank , to review detailed account activity , please login to our internet banking at <a href='".APP_URL."'>".APP_NAME."</a></p> ";
        $body .= "<p>If you require any clarification on this transaction or any of our services , please contact our 24/7  support contact <a href='mailto:support@nestcitybank.com'>support@nestcitybank.com</a> </p>";
        $body .= "</div>";
        return $body;
    }

    public function mailHeader($name)
    {
        $body = "<img src='".ASSETS."main/img/slogo.png' width='300'>";
        $body .= "<hr>";
        $body .= "<h1 style='color:blue;'>Dear $name</h1>";
        return $body;
    }
}
