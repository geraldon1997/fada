<?php
namespace App\Core;

class Request
{
    public static function method()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public static function isGet()
    {
        return self::method() === 'get' ? true : false;
    }

    public static function isPost()
    {
        return self::method() === 'post' ? true : false;
    }

    public static function fullPath()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public static function path()
    {
        $queryPosition = strpos(self::fullPath(), '?');
        if ($queryPosition) {
            return rtrim(substr(self::fullPath(), 0, $queryPosition), '/');
        }

        if (strlen(self::fullPath()) > 1) {
            rtrim(self::fullPath(), '/');
        }
        return self::fullPath();
    }
}
