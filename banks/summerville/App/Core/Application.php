<?php
namespace App\Core;

class Application
{
    public static function start()
    {
        echo Route::init();
        // echo password_hash('123', PASSWORD_BCRYPT);
    }
}

function view($view, array $data = null)
{
    $viewsFolder = 'App/Views/';
    $viewFile = $viewsFolder.$view.'.php';

    if (file_exists($viewFile)) {
        ob_start();
        include_once $viewFile;
        $viewComponent = ob_get_clean();

        $layoutFolder = 'App/Layouts/';
        $layout = explode('/', $view)[0];
        $layout = $layoutFolder.$layout.'.php';

        ob_start();
        include_once $layout;
        $layout = ob_get_clean();
        
        return str_replace('{{content}}', $viewComponent, $layout);
    }

    return "<h1>view <i style='color:blue;'>$view</i> not found</h1>";
}
