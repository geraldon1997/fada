<?php
namespace App\Core;

use App\Controllers\HomeController;
use App\Controllers\UserController;

class Route
{
    public static array $routes;

    public static function get($path, $callback)
    {
        self::$routes['get'][$path] = $callback;
    }

    public static function post($path, $callback)
    {
        self::$routes['post'][$path] = $callback;
    }

    public static function init()
    {
        $path = Request::path();
        $method = Request::method();
        $routes = self::$routes;

        if ($path === '/') {
            return Response::redirect();
        }

        $extra = substr_count($path, '/');

        if ($extra > 1 && $extra < 3) {
            $pathArray = trim($path, '/');
            $pathArray = explode('/', $pathArray);
            $isPath = array_key_exists('/'.$pathArray[0], $routes[$method]);
            $params = $pathArray[1];
        } elseif ($extra === 1) {
            $isPath = array_key_exists($path, $routes[$method]);
            $params = "";
        }

        if ($isPath) {
            if ($extra > 1 && $extra < 3) {
                $callback = array_values($routes[$method]['/'.$pathArray[0]]);
            } elseif ($extra === 1) {
                $callback = array_values($routes[$method][$path]);
            }
            
            $controller = $callback[0];
            $action = $callback[1];
            return call_user_func([new $controller, $action], $params);
        }

        return "<h1>page not found</h1>
                <a href='".GO_BACK."' style='background-color: blue; color: white; text-decoration: none; padding: 10px; border-radius: 5px;'>Go Back</a>";
    }
}
