<?php
namespace App\Core;

use mysqli;

class Gateway
{
    private static function init()
    {
        return new mysqli('localhost', 'root', '', DB_NAME);
    }

    private static function close()
    {
        return self::init()->close();
    }

    protected static function execute($query)
    {
        $result = self::init()->query($query);
        self::close();
        return $result;
    }

    protected static function fetch($query)
    {
        $data = [];

        $result = self::init()->query($query);
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row);
        }

        self::close();

        return $data;
    }

    protected static function tableColumns(string $table)
    {
        // Query to get columns from table
        $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "."'". DB_NAME ."'"." AND TABLE_NAME = '$table' ";
        $query = self::init()->query($query);

        while ($row = $query->fetch_assoc()) {
            $result[] = $row;
        }

        // Array of all column names
        $columnArr = array_column($result, 'COLUMN_NAME');
        return $columnArr;
    }
}
