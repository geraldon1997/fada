<?php
namespace App\Core;

class Response
{
    public static function code(int $code = null)
    {
        return http_response_code($code);
    }

    public static function redirect(string $path = '/home')
    {
        return header('location:'.$path);
    }
}
