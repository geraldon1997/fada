<?php
namespace App\Core;

class Model extends Gateway
{
    private function columns(string $table)
    {
        $columns = self::tableColumns($table);
        return $columns;
    }

    public static function create(array $data)
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $cols = array_keys($data);
        $cols = implode(",", $cols);

        $vals = array_values($data);
        $vals = implode("', '", $vals);
        $vals = "'$vals'";

        $query = "INSERT INTO $table ($cols) VALUES ($vals)";
        return self::execute($query);
    }

    private static function read($table, array $data)
    {
        $cols = array_keys($data);
        $cols = implode("", $cols);

        $vals = array_values($data);
        $vals = implode("", $vals);
        $vals = "'$vals'";

        $query = "SELECT * FROM $table WHERE $cols = $vals";
        return self::fetch($query);
    }

    public static function update(array $data)
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $col = array_pop(array_keys($data));
        $val = array_pop(array_values($data));

        array_pop($data);

        $update = [];

        foreach ($data as $key => $value) {
            array_push($update, $key." = '$value'");
        }

        $set = implode(", ", $update);
        $query = "UPDATE $table SET $set WHERE $col = '$val'";
        return self::execute($query);
    }

    public static function delete(array $data)
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $col = array_keys($data);
        $col = implode("", $col);

        $val = array_values($data);
        $val = implode("", $val);
        
        $query = "DELETE FROM $table WHERE $col = $val";
        return self::execute($query);
    }

    public static function all()
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $query = "SELECT * FROM $table";
        return self::fetch($query);
    }

    public static function allDesc()
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $query = "SELECT * FROM $table ORDER BY id DESC";
        return self::fetch($query);
    }

    public static function first()
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $query = "SELECT * FROM $table ORDER BY id ASC LIMIT 1";
        return self::fetch($query);
    }

    public static function last()
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $query = "SELECT * FROM $table ORDER BY id DESC LIMIT 1";
        return self::fetch($query);
    }

    public static function where($data)
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        return self::read($table, $data);
    }

    public static function find(string $col, string $val)
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $query = "SELECT * FROM $table WHERE $col = '$val' ";
        return self::fetch($query);
    }

    public static function many(string $many)
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        $query = "SELECT * FROM $table WHERE $many";
        return self::fetch($query);
    }

    public static function talk()
    {
        $table = debug_backtrace()[1]['class'];
        $table = strtolower($table);
        $table = explode('\\', $table);
        $table = end($table);
        $table = str_replace('controller', 's', $table);

        return $table;
    }
}
