<?php
namespace App\Routes;

use App\Controllers\AccountController;
use App\Controllers\AuthController;
use App\Controllers\HomeController;
use App\Controllers\ProfileController;
use App\Controllers\TransactionController;
use App\Controllers\UserController;
use App\Core\Route;
use App\Models\Auth;

class Web
{
    public static function register()
    {
        Route::get('/home', [HomeController::class, 'home']);
        Route::get('/about', [HomeController::class, 'about']);
        Route::get('/services', [HomeController::class, 'services']);
        Route::get('/faqs', [HomeController::class, 'faqs']);
        Route::get('/contact', [HomeController::class, 'contact']);
        Route::get('/register', [UserController::class, 'createAccountForm']);
        Route::get('/login', [UserController::class, 'login']);
        Route::get('/forgotpassword', [UserController::class, 'forgotPasswordForm']);
        Route::get('/resetpassword', [UserController::class, 'resetPassword']);

        Route::post('/register', [UserController::class, 'addUser']);
        Route::post('/login', [UserController::class, 'authUser']);
        Route::post('/sendpasswordresetlink', [UserController::class, 'sendPasswordResetLink']);
        Route::post('/resetpassword', [UserController::class, 'setNewPassword']);

        if (isset($_SESSION['login'])) {
            Route::get('/dashboard', [UserController::class, 'dashboard']);
            Route::get('/transfer', [AccountController::class, 'transfer']);
            Route::get('/history', [AccountController::class, 'history']);
            Route::get('/profile', [UserController::class, 'profile']);
            Route::get('/settings', [UserController::class, 'settings']);
            Route::get('/verifytoken', [AuthController::class, 'showform']);

            /******************** post routes *****************************************/
            
            Route::post('/createprofile', [ProfileController::class, 'addProfile']);
            Route::post('/updateprofile', [ProfileController::class, 'updateProfile']);
            Route::post('/checktransferpin', [AuthController::class, 'checkpin']);
            Route::post('/verifytoken', [AuthController::class, 'checktransfertoken']);
            Route::post('/updatepassword', [UserController::class, 'updatePassword']);
            Route::post('/updatepin', [AuthController::class, 'updatePin']);
            Route::post('/resendtoken', [AuthController::class, 'resendTokenToEmail']);

            Route::post('/logout', [UserController::class, 'logoutUser']);

            /********************* admin routes **************************************/

            if ($_SESSION['login'] === 'admin') {
                Route::get('/users', [UserController::class, 'showAll']);
                Route::get('/user', [UserController::class, 'show']);
                Route::get("/banuser", [UserController::class, 'ban']);
                Route::get('/unbanuser', [UserController::class, 'unban']);
                Route::get('/lockaccount', [AccountController::class, 'lock']);
                Route::get('/unlockaccount', [AccountController::class, 'unlock']);
                Route::get("/deleteuser", [UserController::class, 'delete']);
                Route::get("/credituser", [AccountController::class, 'showCreditForm']);

                Route::post("/credituser", [AccountController::class, 'creditUser']);
            }
        }
    }
}
