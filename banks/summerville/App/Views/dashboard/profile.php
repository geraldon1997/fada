<div class="form-head mb-4">
    <h2 class="text-black font-w600 mb-0">Dashboard / Profile</h2>
</div>

<div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Account Information</h4>
            </div>
            
            <div class="card-body">
                <div class="basic-form">
                    <form action="<?= $data ? UPDATE_PROFILE : CREATE_PROFILE; ?>" method="post" enctype="multipart/form-data">

                        <h4 class="text-black font-w600">Personal Information:</h4>
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Last name*</span>
                                    </div>
                                    <input type="text" name="ln" class="form-control" placeholder="Last name" value="<?= (count($data) > 0) ? $data[0]['lastname'] : '' ?>">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">First name*</span>
                                    </div>
                                    <input type="text" name="fn" class="form-control" placeholder="First name" value="<?= (count($data) > 0) ? $data[0]['firstname'] : '' ?>">
                                </div>
                            </div>

                        </div>
                        
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Gender*</span>
                                    </div>
                                    <select name="gender" id="" class="form-control">
                                        <option value="<?= $data[0]['gender'] ? $data[0]['gender'] : '' ?>"> <?= $data[0]['gender'] ? $data[0]['gender'] : 'Gender' ?> </option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">Date of birth*</span>
                                    </div>
                                    <input type="date" name="dob" class="form-control" placeholder="Date of birth"  value="<?= (count($data) > 0) ? $data[0]['dob'] : '' ?>">
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Marital Status*</span>
                                    </div>
                                    <select name="ms" id="" class="form-control">
                                        <option value="<?= $data[0]['marital_status'] ? $data[0]['marital_status'] : '' ?>">  <?= $data[0]['marital_status'] ? $data[0]['marital_status'] : 'Gender' ?> </option>
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="divorced">Divorced</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">Country of origin*</span>
                                    </div>
                                    <input type="text" name="co" class="form-control" placeholder="Country of origin" value="<?= (count($data) > 0) ? $data[0]['country_of_origin'] : '' ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-row mb-4">
                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">State / Province*</span>
                                    </div>
                                    <input type="text" name="so" class="form-control" placeholder="State / Province" value="<?= (count($data) > 0) ? $data[0]['state_of_origin'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                
                                    <div class="input-group-append">
                                        <span class="input-group-text">Phone*</span>
                                    </div>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone" value="<?= (count($data) > 0) ? $data[0]['phone'] : '' ?>">
                                </div>
                            </div>
                        </div>

                        <h4 class="text-black font-w600">Contact Information:</h4>
                        <div class="form-row mb-3">
                            <div class="col-md-12">
                                <div class="input-group input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Residential Address</span>
                                    </div>
                                    <textarea name="res" class="form-control" placeholder="Address"> <?= (count($data) > 0) ? $data[0]['residence'] : '' ?> </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">State / Province *</span>
                                    </div>
                                    <input type="text" name="sr" class="form-control" placeholder="State / Province" value="<?= (count($data) > 0) ? $data[0]['state_of_residence'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">Country*</span>
                                    </div>
                                    <input type="text" name="cr" class="form-control" placeholder="Country" value="<?= (count($data) > 0) ? $data[0]['country_of_residence'] : '' ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Occupation*</span>
                                    </div>
                                    <input type="text" name="occu" class="form-control" placeholder="Occupation" value="<?= (count($data) > 0) ? $data[0]['occupation'] : '' ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    
                                    <div class="custom-file">
                                        <input type="file" name="id" class="custom-file-input" >
                                        <label class="custom-file-label font-w600">Upload Valid ID</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                            <button class="btn btn-success btn-square">Update</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
