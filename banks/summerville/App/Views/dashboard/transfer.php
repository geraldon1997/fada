    <div class="form-head mb-4">
		<h2 class="text-black font-w600 mb-0">Dashboard / Transfer</h2>
	</div>

    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Make a transfer</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="#">

                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Bank name</span>
                                    </div>
                                    <input type="text" class="form-control" id="bank" placeholder="Bank name" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Bank address</span>
                                    </div>
                                    <input type="text" class="form-control" id="baddr" placeholder="Bank address"  required>
                                </div>
                            </div>

                            

                        </div>
                        
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> Routing / Swift Code </span>
                                    </div>
                                    <input type="text" class="form-control" id="route" placeholder="Routing / Swift Code" required>
                                    
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> Receiver's name </span>
                                    </div>
                                    <input type="text" class="form-control" id="rn" placeholder="Receiver's name" required>
                                    
                                </div>
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Account Number</span>
                                    </div>
                                    <input type="number" class="form-control" id="an" placeholder="Account Number" required>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> $ </span>
                                    </div>
                                    <input type="number" class="form-control" id="amount" placeholder="Amount" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text"> .00 </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Transfer Type</span>
                                    </div>
                                    <select id="transfer" class="form-control" required>
                                        <option value="">Choose Transfer Type</option>
                                        <option value="local">Local Transfer</option>
                                        <option value="international">International Transfer</option>
                                        <option value="domestic">Domestic Transfer</option>
                                    </select>  
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="input-group input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> Date of Transfer </span>
                                    </div>
                                    <input type="date" id="dot" class="form-control" placeholder="date of transfer" required>
                                </div>
                            </div>

                        </div>

                        <div class="form-row mb-4">
                            <div class="col-md-12">
                                <div class="input-group input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Purpose of transfer</span>
                                    </div>
                                    <input type="text" id="purpose" class="form-control" placeholder="Purpose of transfer" required>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-square btn-success" id="tb">Process transfer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script>
    $('form').submit((e) => {
        e.preventDefault();
        let pin = prompt('Enter your tansfer pin');
        
        if (pin === null) {
            return;
        }else if (pin === '' || pin === ' ' || pin === undefined){
            return alert('please enter your transfer pin');
        } else {
            let btn = $('#tb');
            btn.attr('disabled', true).html('processing . . ');

            let bank = $('#bank').val();
            let bankaddr = $('#baddr').val();
            let route = $('#route').val();
            let rname = $('#rn').val();
            let acctnum = $('#an').val();
            let amount = $('#amount').val();
            let transfer = $('#transfer').val();
            let dot = $('#dot').val();
            let purpose = $('#purpose').val();

            $.ajax({
                type: 'POST',
                url: "<?= CHECK_TRANSFER_PIN; ?>",
                data:{
                    pincode: pin,
                    bank: bank,
                    bankaddr: bankaddr,
                    route :route,
                    rname: rname,
                    acctnum: acctnum,
                    amount: amount,
                    dot: dot,
                    transfer: transfer,
                    purpose: purpose
                }
            }).done(result => {
                // return console.log(result);
                if (result === 'error_pin') {
                    btn.removeAttr('disabled').html('Process transfer');
                    return alert('Invalid Transfer Pin');
                } else if (result === 'error_account_locked') {
                    btn.removeAttr('disabled').html('Process transfer');
                    return alert('Account is Locked, please contact support')
                } else if (result === 'error_balance_low') {
                    btn.removeAttr('disabled').html('Process transfer');
                    return alert('OOps! You cannot transfer below $100');
                } else if (result === 'error_balance_high') {
                    btn.removeAttr('disabled').html('Process transfer');
                    return alert('OOps! Insufficient balance');
                } else if (result === 'success') {
                    alert('A token has been sent to your email to complete the transfer');
                    return location = "<?= VERIFY_TOKEN; ?>"
                }
            });
        }
        
    })
</script>