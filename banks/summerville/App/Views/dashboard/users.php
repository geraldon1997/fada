<?php

use App\Controllers\AccountController;

$sn = 1;
$account = new AccountController;
?>
<div class="form-head mb-4">
    <h2 class="text-black font-w600 mb-0">Dashboard / Users</h2>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive table-hover fs-14 card-table">
            <div id="example5_wrapper" class="dataTables_wrapper no-footer"><table class="table display mb-4 dataTablesCard dataTable no-footer" id="example5" role="grid" aria-describedby="example5_info">
                <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" aria-label="ID Invoice: activate to sort column ascending" style="width: 78px;">SN</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 156px;">Email</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" aria-label="Recipient: activate to sort column ascending" style="width: 181px;">Login ID</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" aria-label="Amount: activate to sort column ascending" style="width: 72.5px;">Action</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $users) : ?>
                    <tr role="row" class="odd">
                        <td><?= $sn++; ?></td>
                        <td><?= $users['email']; ?></td>
                        <td><?= $users['login_id']; ?></td>
                        <td>
                            <a href="<?= VIEW_USER.'/'.$users['id']; ?>" title="view user" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="<?= CREDIT_USER.'/'.$users['id']; ?>" title="Credit user" class="btn btn-success btn-sm"><i class="fa fa-money"></i></a>
                            
                            <?php if ($account->isLocked($users['id'])) : ?>
                                <a href="<?= UNLOCK_ACCOUNT.'/'.$users['id']; ?>" title="unlock user account" class="btn btn-secondary btn-sm"><i class="fa fa-unlock"></i></a>
                            <?php else : ?>
                                <a href="<?= LOCK_ACCOUNT.'/'.$users['id']; ?>" title="lock user account" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>
                            <?php endif; ?>
                            
                            <?php if ($users['is_banned']) : ?>
                                <a href="<?= UNBAN_USER.'/'.$users['id']; ?>" title="unban user" class="btn btn-info btn-sm"><i class="fa fa-arrow-circle-up"></i></a>
                            <?php else : ?>
                                <a href="<?= BAN_USER.'/'.$users['id']; ?>" title="ban user" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-down"></i></a>
                            <?php endif; ?>
                            
                            <a href="<?= DELETE_USER.'/'.$users['id']; ?>" title="delete user" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>