<div class="form-head mb-4">
		<h2 class="text-black font-w600 mb-0">Dashboard / Transfer</h2>
	</div>

    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Credit User</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="<?= CREDIT_USER; ?>" method="post">
                        <input type="hidden" name="userid" value="<?= $data[0]; ?>">
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Bank name</span>
                                    </div>
                                    <input type="text" class="form-control" name="bank" id="bank" placeholder="Bank name" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Bank address</span>
                                    </div>
                                    <input type="text" class="form-control" name="baddr" id="baddr" placeholder="Bank address"  required>
                                </div>
                            </div>

                            

                        </div>
                        
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> Routing / Swift Code </span>
                                    </div>
                                    <input type="text" class="form-control" name="route" id="route" placeholder="Routing / Swift Code" required>
                                    
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> Receiver's name </span>
                                    </div>
                                    <input type="text" class="form-control" name="rn" id="rname" placeholder="Receiver's name" required>
                                    
                                </div>
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Account Number</span>
                                    </div>
                                    <input type="number" class="form-control" name="an" id="an" placeholder="Account Number" required>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text"> $ </span>
                                    </div>
                                    <input type="number" name="amount" class="form-control" id="amount" placeholder="Amount" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text"> .00 </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3  input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Transfer Type</span>
                                    </div>
                                    <select id="transfer" name="transfer" class="form-control" required>
                                        <option value="">Choose Transfer Type</option>
                                        <option value="local">Local Transfer</option>
                                        <option value="international">International Transfer</option>
                                        <option value="domestic">Domestic Transfer</option>
                                    </select>  
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="input-group input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> Date of Transfer </span>
                                    </div>
                                    <input type="date" name="dot" id="dot" class="form-control" placeholder="date of transfer" required>
                                </div>
                            </div>

                        </div>

                        <div class="form-row mb-4">
                            <div class="col-md-12">
                                <div class="input-group input-primary">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Purpose of transfer</span>
                                    </div>
                                    <input type="text" name="purpose" id="purpose" class="form-control" placeholder="Purpose of transfer" required>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-square btn-success" id="tb">Process transfer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>