<div class="form-head mb-4">
    <h2 class="text-black font-w600 mb-0">Dashboard / Account Settings</h2>
</div>

<div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Account Settings</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="/updatepassword" class="mb-5">

                        <h4 class="text-black font-w600">Update Password:</h4>
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Old Password</span>
                                    </div>
                                    <input type="text" class="form-control" name="op" placeholder="Old Password" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">New Password*</span>
                                    </div>
                                    <input type="text" class="form-control" name="np" placeholder="New Password" required>
                                </div>
                            </div>

                        </div>
                        
                        

                        <button class="btn btn-success btn-square">Update</button>
                    </form>

                    <form action="/updatepin">

                        <h4 class="text-black font-w600">Update Transfer Pin:</h4>
                        <div class="form-row">

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Old Transfer Pin</span>
                                    </div>
                                    <input type="text" class="form-control" name="otp" placeholder="Old Transfer Pin" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group mb-3 input-primary">
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">New Transfer Pin</span>
                                    </div>
                                    <input type="text" class="form-control" name="ntp" placeholder="New Transfer Pin" required>
                                </div>
                            </div>

                        </div>
                        
                        

                        <button class="btn btn-success btn-square">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>