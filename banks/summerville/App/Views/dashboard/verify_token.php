<div class="form-head mb-4">
		<h2 class="text-black font-w600 mb-0">Dashboard / Verify Token</h2>
	</div>

    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Enter token to complete transfer</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="#">

                        <div class="col-md-6 offset-3">
                            <div class="input-group mb-3  input-primary">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Token</span>
                                </div>
                                <input type="number" class="form-control" id="token" placeholder="Token" value="" required>
                                
                            </div>
                            <button type="submit" class="btn btn-square btn-success" id="verify">Verify</button>
                        </div>  

                        
                    </form>
                    <button class="btn btn-primary" id="resend">Resend Token</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('form').submit((e) => {
            e.preventDefault();
            
            let token = $('#token').val();
            let btn = $('#verify');
                btn.attr('disabled', true).html('processing . . .');

            $.ajax({
                type: 'POST',
                url: "<?= VERIFY_TOKEN; ?>",
                data: {
                    token: token
                }
            }).done(result => {
                if (result === 'error') {
                    return alert('OOps! Invalide token');
                } else if (result === 'success') {
                    alert('Transfer was successful');
                    return location = "<?= DASHBOARD; ?>";
                }
            })
        });

        $('#resend').click(() => {
            $.ajax({
                type: 'POST',
                url: "<?= RESEND_TOKEN; ?>"
            }).done(result => {
                return alert(result);
            })
        })
    </script>