<?php

use App\Controllers\UserController;

$user = new UserController;
?>

<?php if ($user->hasProfile() || $_SESSION['login'] === 'admin') : ?>
<!-- row -->
    <div class="form-head mb-4">
        <h2 class="text-black font-w600 mb-0">Dashboard / Account Summary</h2>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card-bx stacked">
                        <img src="<?= ASSETS; ?>dashboard/images/card/card.png" alt="<?= APP_NAME; ?>" class="mw-100">
                        <div class="card-info text-white">
                            <p class="mb-1">Account Balance</p>
                            <h2 class="fs-36 text-white mb-4">$<?= number_format($data['account']['account_balance']) ?></h2>
                            
                            <div class="d-flex">
                                <div class="mr-5">
                                    <p class="fs-14 mb-1 op6">Account Name</p>
                                    <span> <?= ucfirst($data['name']['lastname'].' '.$data['name']['firstname']) ?> </span>
                                </div>
                                <div>
                                    <p class="fs-14 mb-1 op6">Account Number</p>
                                    <span> <?= $data['account']['account_number'] ?> </span>
                                </div>
                            </div>
                        </div>
                        <a href="<?= HISTORY; ?>"><i class="fa fa-caret-down" aria-hidden="true"></i></a>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
<?php else : ?>
    <div class="alert alert-primary solid alert-dismissible fade show">
        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg>
        <strong>Welcome!</strong> Please Click <a class="btn btn-dark" href="<?= PROFILE; ?>">here</a> to complete your registration
        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
        </button>
    </div>
<?php endif; ?>