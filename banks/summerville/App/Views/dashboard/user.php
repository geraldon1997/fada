<?php
$sn = 1;
?>

<div class="form-head mb-4">
    <h2 class="text-black font-w600 mb-0">Dashboard / User</h2>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive table-hover fs-14 card-table">
            <div id="example5_wrapper" class="dataTables_wrapper no-footer"><table class="table display mb-4 dataTablesCard dataTable no-footer" id="example5" role="grid" aria-describedby="example5_info">
                <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >SN</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >Fullname</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >gender</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >dob</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >marital status</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >phone</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >place of origin</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >residence</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >occupation</th>
                        <th class="sorting" tabindex="0" aria-controls="example5" rowspan="1" colspan="1" >photo</th>
                    </tr>
                </thead>
                <tbody>             
                <?php foreach ($data as $user) : ?>                 
                    <tr role="row" class="odd">
                        <td><?= $sn++; ?></td>
                        <td><?= $user['lastname'].' '.$user['firstname']; ?></td>
                        <td><?= $user['gender']; ?></td>
                        <td><?= $user['dob']; ?></td>
                        <td><?= $user['marital_status']; ?></td>
                        <td><?= $user['phone']; ?></td>
                        <td><?= $user['state_of_origin'].', '.$user['country_of_origin']; ?></td>
                        <td><?= trim($user['residence'].', '.$user['state_of_residence'].', '.$user['country_of_residence']) ?></td>
                        <td><?= $user['occupation'] ?></td>
                        <td><img src="<?= '/'.$user['id_url']; ?>" width="200"></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>