<div class="main-banner jarallax" data-jarallax='{"speed": 0.3}'>
<div class="d-table">
<div class="d-table-cell">
<div class="container">
<div class="main-banner-content">
<h1>Easy and fee-free banking for everyone</h1>
<p>Building relationships with quality banking services.</p>
<a href="<?= REGISTER;?>" class="btn btn-primary">Open An Account</a>
</div>
</div>
</div>
</div>
</div>


<section class="featured-boxes-area">
<div class="container">
<div class="featured-boxes-inner">
<div class="row m-0">
<div class="col-lg-3 col-sm-6 col-md-6 p-0">
<div class="single-featured-box">
<div class="icon color-fb7756">
<i class="flaticon-piggy-bank"></i>
</div>
 <h3>Transparent Pricing</h3>
<p>We are well known for our tranparent services, nothing is hidden under the sun.</p>
<span class="fa fa-eye"></span>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-md-6 p-0">
<div class="single-featured-box">
<div class="icon color-facd60">
<i class="flaticon-data-encryption"></i>
</div>
<h3>Fully Encrypted</h3>
<p>Our banking services are fully secured with encryption keys for any transaction type.</p>
<span class="fa fa-key"></span>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-md-6 p-0">
<div class="single-featured-box">
<div class="icon color-1ac0c6">
<i class="flaticon-wallet"></i>
</div>
<h3>Instant Cashout</h3>
<p>Always readily available to serve and offer you easy, fast, reliable and secured cashout policies.</p>
<span class="fa fa-credit-card"></span>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-md-6 p-0">
<div class="single-featured-box">
<div class="icon">
<i class="flaticon-shield"></i>
</div>
<h3>Safe and Secure</h3>
<p>Your funds are safe and fully secured with us, we notify you if unusual activities occur in your account.</p>
<span class="fa fa-lock"></span>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="services-area ptb-70 bg-f7fafd">
<div class="container-fluid p-0">
<div class="overview-box">
<div class="overview-image">
<div class="image">
<img src="<?= ASSETS; ?>main/img/2.png" alt="image">
<div class="circle-img">
<img src="<?= ASSETS; ?>main/img/circle.png" alt="image">
</div>
</div>
</div>
<div class="overview-content">
<div class="content">
<h2>Business Loans</h2>
<div class="bar"></div>
<p>We support local and international businesses, including large and small scale establishments</p>
<ul class="services-list">
<li><span><i class="flaticon-check-mark"></i> Easy transactions</span></li>
<li><span><i class="flaticon-check-mark"></i> Deposit checks instantly</span></li>
<li><span><i class="flaticon-check-mark"></i> A powerful open gateway</span></li>
<li><span><i class="flaticon-check-mark"></i> Coverage around the world</span></li>
<li><span><i class="flaticon-check-mark"></i> Business without borders</span></li>
<li><span><i class="flaticon-check-mark"></i> Affiliates and partnerships</span></li>
</ul>
</div>
</div>
</div>
</div>
</section>

<section class="features-area ptb-70 bg-f6f4f8">
<div class="container">
<div class="section-title">
<h2>Our Features</h2>
<div class="bar"></div>
<p>Our Online Banking is available 24/7, you can easily send money anywhere in the world, view and track account transactions, download and manage account statements.</p>
</div>
<div class="row">
<div class="col-lg-4 col-sm-6 col-md-6">
<div class="single-features-box">
<div class="icon">
<i class="flaticon-envelope-of-white-paper"></i>
</div>
<h3>Email notfications</h3>
<p>We notify you through email on any activity been carried out on your account to enhance safety and fund security.</p>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-md-6">
<div class="single-features-box">
<div class="icon bg-f78acb">
<i class="flaticon-settings"></i>
</div>
<h3>Quick setup</h3>
<p>Easily set up your account and be able to send and receive money. account set up just got easier, reliable and quick.</p>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-md-6">
<div class="single-features-box">
<div class="icon bg-cdf1d8">
<i class="flaticon-menu"></i>
</div>
<h3>User friendly</h3>
<p>We have have a user friendly online banking system which makes our banking processes unique. </p>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-md-6">
<div class="single-features-box">
<div class="icon bg-c679e3">
<i class="flaticon-info"></i>
</div>
<h3>Information retrieval</h3>
<p>Information on all your account transactions both debits and credits are quickly retrieved just on the go for reference purposes.</p>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-md-6">
<div class="single-features-box">
<div class="icon bg-eb6b3d">
<i class="flaticon-cursor"></i>
</div>
<h3>Interactive</h3>
<p>Our banking system is highly interactive, strictly guiding you to avoid making errors on any transaction.</p>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-md-6">
<div class="single-features-box">
<div class="icon">
<i class="flaticon-alarm"></i>
</div>
<h3>Notification reminders</h3>
<p>Enable notifications to get notified on any newly added features or newly added.</p>
</div>
</div>
</div>
</div>
</section>

<section class="invoicing-area ptb-70">
<div class="container-fluid">
<div class="row align-items-center">
<div class="col-lg-6 col-md-12">
<div class="invoicing-content">
<h2>Solving Your Financial Problems</h2>
 <div class="bar"></div>
<p>Just like the satellite watches over the earth... Our watch is centered over all your financial transactions enabling two step authentication for transaction security. <?= APP_NAME;?> is the go to for you..</p>
<a href="<?= LOGIN?>" class="btn btn-primary">Get Started</a>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="invoicing-image">
<div class="main-image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/1.png" class="wow zoomIn" data-wow-delay="0.7s" alt="image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/2.png" class="wow fadeInLeft" data-wow-delay="1s" alt="image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/3.png" class="wow fadeInLeft" data-wow-delay="1.3s" alt="image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/4.png" class="wow fadeInRight" data-wow-delay="1s" alt="image">
</div>
<div class="main-mobile-image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/main-pic.png" class="wow zoomIn" data-wow-delay="0.7s" alt="image">
</div>
<div class="circle-image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/circle1.png" alt="image">
<img src="<?= ASSETS; ?>main/img/invoicing-image/circle2.png" alt="image">
</div>
</div>
</div>
</div>
</div>
</section>


<section class="funfacts-area ptb-70 pt-0">
<div class="container">
<div class="section-title">
<h2>We always try to understand customers expectation</h2>
<div class="bar"></div>
<p><?= APP_NAME;?> banking experts are available for 24/7 consultation, booking a date for consult just got easier.</p>
</div>

<div class="contact-cta-box">
<h3>Have any question about us?</h3>
<p>Don't hesitate to contact us</p>
<a href="<?= CONTACT;?>" class="btn btn-primary">Contact Us</a>
</div>
<!-- <div class="map-bg">
<img src="<?= ASSETS; ?>main/img/map.png" alt="map">
</div> -->
</div>
</section>