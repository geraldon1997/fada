

<div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
<div class="container">
<div class="page-title-content">
<h2>About Us</h2>
<p><?= APP_NAME;?></p>
</div>
</div>
</div>


<section class="about-area ptb-70">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6 col-md-12">
<div class="about-content">
<h2>Easy, fee-free banking for entrepreneurs</h2>
<p>We've made it easy for you to do your banking 24 hours a day, 7 days a week, from your living room couch, a coffee shop, out in the fields, or elsewhere you can access the internet.</p>
<p>Long term, consistent investing is critical in wealth management & financial planning These are typically debt instruments issued by corporations or governments to raise long term capital to finance capital projects.</p>
<p>Every month they moved their money the old way – which wasted their time and money. So they invented a beautifully simple workaround that became a billion-dollar business.</p>
<p>If you are a business owner that has big plans for growth, you'll need a banking partner that can provide lending expertise and responsive service.</p>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="about-image">
<img src="<?= ASSETS; ?>main/img/about-img1.jpg" alt="image">
<a href="https://www.youtube.com/watch?v=bk7McNUjWgw" class="video-btn popup-youtube"><i class="fas fa-play"></i></a>
</div>
</div>
</div>
</div>
</section>
