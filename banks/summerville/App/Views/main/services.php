<div class="page-title-section">
    <div class="container">
    <div class="page-title-text">
    <h2>Services we offer</h2>
    <p>Our Services</p>
    <ul>
    <li><a href="<?= HOME;?>">Home</a></li>
    <li>Services</li>
    </ul>
    </div>
    </div>
</div>

<section class="services-area ptb-70">
<div class="container-fluid p-0">
<div class="overview-box">
<div class="overview-content">
<div class="content left-content">
<span class="sub-title">Fast & Easy Online Banking for</span>
<h2>Freelancers, entrepreneurs, and sole traders</h2>
<div class="bar"></div>
<p>Looking for an easy and efficient way to do business transactions <?= APP_NAME;?> is the go for you.</p>
<ul class="features-list">
<li><span><i class="flaticon-check-mark"></i> Free plan available</span></li>
<li><span><i class="flaticon-check-mark"></i> Full data privacy compliance</span></li>
<li><span><i class="flaticon-check-mark"></i> 100% transparent costs</span></li>
<li><span><i class="flaticon-check-mark"></i> Commitment-free</span></li>
<li><span><i class="flaticon-check-mark"></i> Real-time spending overview</span></li>
<li><span><i class="flaticon-check-mark"></i> Debit and Credit Mastercard included</span></li>
</ul>
<a href="<?= LOGIN;?>" class="btn btn-primary">Apply Now</a>
</div>
</div>
<div class="overview-image">
<div class="image">
<img src="<?= ASSETS; ?>main/img/4.jpg" alt="image">
</div>
</div>
</div>
</div>
</section>


<section class="services-area ptb-70 bg-f7fafd">
<div class="container-fluid p-0">
<div class="overview-box">
<div class="overview-image">
<div class="image">
<img src="<?= ASSETS; ?>main/img/5.png" alt="image">
<div class="circle-img">
<img src="<?= ASSETS; ?>main/img/circle.png" alt="image">
</div>
</div>
</div>
<div class="overview-content">
<div class="content">
<span class="sub-title">Top Security</span>
<h2>Secured systems</h2>
<div class="bar"></div>
<p>Highly secured transactions for individuals and business owners.</p>
<ul class="features-list">
<li><span><i class="flaticon-check-mark"></i> Easy transfers</span></li>
<li><span><i class="flaticon-check-mark"></i> Deposit checks instantly</span></li>
<li><span><i class="flaticon-check-mark"></i> A powerful open API</span></li>
<li><span><i class="flaticon-check-mark"></i> Coverage around the world</span></li>
<li><span><i class="flaticon-check-mark"></i> Business without borders</span></li>
<li><span><i class="flaticon-check-mark"></i> Affiliates and partnerships</span></li>
</ul>
<a href="<?= LOGIN;?>" class="btn btn-primary">Get Started</a>
</div>
</div>
</div>
</div>
</section>


<section class="services-area ptb-70">
<div class="container-fluid p-0">
<div class="overview-box">
<div class="overview-content">
<div class="content left-content">
<span class="sub-title">Price Transparency for</span>
<h2>Large or enterprise level businesses</h2>
<div class="bar"></div>
<p>Service charge and percentages are very transparent.</p>
<ul class="features-list">
<li><span><i class="flaticon-check-mark"></i> Corporate Cards</span></li>
<li><span><i class="flaticon-check-mark"></i> International Payments</span></li>
<li><span><i class="flaticon-check-mark"></i> Automated accounting</span></li>
<li><span><i class="flaticon-check-mark"></i> Request Features</span></li>
<li><span><i class="flaticon-check-mark"></i> Premium Support</span></li>
<li><span><i class="flaticon-check-mark"></i> Direct Debit</span></li>
</ul>
<a href="<?= REGISTER;?>" class="btn btn-primary">Create Account</a>
</div>
</div>
<div class="overview-image">
<div class="image">
<img src="<?= ASSETS; ?>main/img/6.jpg" alt="image">
</div>
</div>
</div>
</div>
</section>


<section class="services-area ptb-70 bg-f7fafd">
<div class="container-fluid p-0">
<div class="overview-box">
<div class="overview-image">
<div class="image">
<img src="<?= ASSETS; ?>main/img/7.jpg" alt="image">
</div>
</div>
<div class="overview-content">
<div class="content">
<span class="sub-title">Automated Accounting</span>
<h2>Save 24 hours per week on accounting</h2>
<div class="bar"></div>
<p>Accounting details are automated for easy and quick access.</p>
<ul class="features-list">
<li><span><i class="flaticon-check-mark"></i> Easy transfers</span></li>
<li><span><i class="flaticon-check-mark"></i> Deposit checks instantly</span></li>
<li><span><i class="flaticon-check-mark"></i> A powerful open API</span></li>
<li><span><i class="flaticon-check-mark"></i> Coverage around the world</span></li>
<li><span><i class="flaticon-check-mark"></i> Business without borders</span></li>
<li><span><i class="flaticon-check-mark"></i> Affiliates and partnerships</span></li>
</ul>
<a href="<?= LOGIN;?>" class="btn btn-primary">Learn More</a>
</div>
</div>
</div>
</div>
</section>

<section class="ready-to-talk">
<div class="container">
<div class="ready-to-talk-content">
<h3>Ready to talk?</h3>
<p>Our team of financial advisors are here to answer your questions at <?php echo APP_NAME;?></p>
<a href="<?= CONTACT;?>" class="btn btn-primary">Contact Us</a>
</div>
</div>
</section>
