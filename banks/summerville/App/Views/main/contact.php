

<div class="page-title-area item-bg2 jarallax" data-jarallax='{"speed": 0.3}'>
<div class="container">
<div class="page-title-content">
<h2>Contact</h2>
<p>We accept complaints and ideas.</p>
</div>
</div>
</div>


<section class="contact-area ptb-70">
<div class="container">
<div class="section-title">
<h2>Drop a message with us</h2>
<div class="bar"></div>
<p>Having banking issues? your issues are resolved immediately you contact us. we accept relatable banking ideas.</p>
</div>
<div class="row">
<div class="col-lg-5 col-md-12">
<div class="contact-info">
<ul>
<li>
<div class="icon">
<i class="fas fa-map-marker-alt"></i>
</div>
 <span>Address</span>
CA 560 Bush St & 20th Ave, Apt 5 San Francisco, 230909, Canada
</li>
<li>
<div class="icon">
<i class="fas fa-envelope"></i>
</div>
<span>Email</span>
<a href="https://templates.envytheme.com/cdn-cgi/l/email-protection#543c3138383b143821223d3b3a7a373b39"><span class="__cf_email__" data-cfemail="b7dfd2dbdbd8f7dbc2c1ded8d999d4d8da">[email&#160;protected]</span></a>
<a href="https://templates.envytheme.com/cdn-cgi/l/email-protection#1f736a697670715f78727e7673317c7072"><span class="__cf_email__" data-cfemail="19756c6f707677597e74787075377a7674">[email&#160;protected]</span></a>
</li>
<li>
<div class="icon">
<i class="fas fa-phone-volume"></i>
</div>
<span>Phone</span>
<a href="tel:+44587154756">+44 587 154756</a>
<a href="tel:+55555514574">+55 5555 14574</a>
</li>
</ul>
</div>
</div>
<div class="col-lg-7 col-md-12">
<div class="contact-form">
<form id="contactForm">
<div class="row">
<div class="col-lg-6 col-md-6">
<div class="form-group">
<input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" placeholder="Name">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-6 col-md-6">
<div class="form-group">
<input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" placeholder="Email">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-6 col-md-6">
<div class="form-group">
<input type="text" name="phone_number" id="phone_number" required data-error="Please enter your number" class="form-control" placeholder="Phone">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-6 col-md-6">
<div class="form-group">
<input type="text" name="msg_subject" id="msg_subject" class="form-control" required data-error="Please enter your subject" placeholder="Subject">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<div class="form-group">
<textarea name="message" class="form-control" id="message" cols="30" rows="6" required data-error="Write your message" placeholder="Your Message"></textarea>
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<button type="submit" class="btn btn-primary">Send Message</button>
 <div id="msgSubmit" class="h3 text-center hidden"></div>
<div class="clearfix"></div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="bg-map"><img src="<?= ASSETS; ?>main/img/bg-map.png" alt="image"></div>
</section>

