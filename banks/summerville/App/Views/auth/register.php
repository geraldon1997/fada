
<section class="signup-area">
<div class="row m-0">
<div class="col-lg-6 col-md-12 p-0">
<div class="signup-image">
<img src="<?= ASSETS; ?>main/img/signup-bg.jpg" alt="image">
</div>
</div>
<div class="col-lg-6 col-md-12 p-0">
<div class="signup-content">
<div class="d-table">
<div class="d-table-cell">
<div class="signup-form">
<div class="logo">
<a href="<?= APP_URL; ?>"><img src="<?= ASSETS; ?>main/img/slogo.png" alt="image" width="100" class="mt-4"></a>
</div>
<h3>Open up your summerville Account now</h3>
<p>Already signed up? <a href="<?= LOGIN; ?>">Log in</a></p>
<form>
<div class="form-group">
<input type="email" name="email" id="email" placeholder="Your email address" class="form-control" required>
</div>

<button type="submit" class="btn btn-primary">Sign Up</button>

</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script>
    $('form').submit((e) => {
        e.preventDefault();
        
        let mail = $('#email').val();

        $.ajax({
            type: "POST",
            url: "<?= REGISTER; ?>",
            data: {
                email: mail
            }
        }).done(result => {
            if (result === 'success') {
                alert('Account creaated, please check your email');
                location = "<?= LOGIN; ?>";
            } else {
                alert('An error occurred, please try again later');
            }
        })
    });
</script>