

<section class="login-area">
<div class="row m-0">
<div class="col-lg-6 col-md-12 p-0">
<div class="login-image">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="image">
</div>
</div>
<div class="col-lg-6 col-md-12 p-0">
<div class="login-content">
<div class="d-table">
<div class="d-table-cell">
<div class="login-form">
<div class="logo">
<a href="<?= APP_URL; ?>"><img src="<?= ASSETS; ?>main/img/slogo.png" alt="image" width="100" class="mt-4"></a>
</div>
<h3>Welcome back</h3>
<p>New to summerville? <a href="<?= REGISTER; ?>">Sign up</a></p>
<form>
<div class="form-group">
<input type="text" name="email" id="login" placeholder="Your Login ID" class="form-control" required>
</div>
<div class="form-group">
<input type="password" name="password" id="password" placeholder="Your password" class="form-control" required>
</div>
<button type="submit" class="btn btn-primary">Login</button>
<div class="forgot-password">
<a href="<?= FORGOT_PASSWORD; ?>">Forgot Password?</a>
</div>
 
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script>
    $('form').submit((e) => {
        e.preventDefault();

        let login = $('#login').val();
        let pass = $('#password').val();

        $.ajax({
            type: 'POST',
            url: "<?= LOGIN; ?>",
            data: {
                login_id: login,
                password: pass
            }
        }).done(result => {
            if (result === 'success') {
                return location = "<?= DASHBOARD; ?>";
            } else {
                return alert(result);
            }
        })
    });
</script>