

<section class="login-area">
<div class="row m-0">
<div class="col-lg-6 col-md-12 p-0">
<div class="login-image">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="image">
</div>
</div>
<div class="col-lg-6 col-md-12 p-0">
<div class="login-content">
<div class="d-table">
<div class="d-table-cell">
<div class="login-form">
<div class="logo">
<a href="<?= APP_URL; ?>"><img src="<?= ASSETS; ?>main/img/slogo.png" alt="image" width="100" class="mt-4"></a>
</div>
<h3> Forgot Password </h3>

<form>
<div class="form-group">
<input type="email" name="email" id="email" placeholder="Your Email Address" class="form-control" required>
</div>

<button type="submit" class="btn btn-primary">reset</button>
 
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script>
    $('form').submit((e) => {
        e.preventDefault();

        let email = $('#email').val();

        $.ajax({
            type: 'POST',
            url: "<?= SEND_PASSWORD_RESET_LINK; ?>",
            data: {
                email: email
            }
        }).done(result => {
            return alert(result);
        })
    });
</script>