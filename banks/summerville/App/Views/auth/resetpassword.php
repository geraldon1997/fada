

<section class="login-area">
<div class="row m-0">
<div class="col-lg-6 col-md-12 p-0">
<div class="login-image">
<img src="<?= ASSETS; ?>main/img/slogo.png" alt="image">
</div>
</div>
<div class="col-lg-6 col-md-12 p-0">
<div class="login-content">
<div class="d-table">
<div class="d-table-cell">
<div class="login-form">
<div class="logo">
<a href="<?= APP_URL; ?>"><img src="<?= ASSETS; ?>main/img/slogo.png" alt="image" width="100" class="mt-4"></a>
</div>
<h3> Reset Password </h3>

<form>
<div class="form-group">
<input type="password" name="password" id="password" placeholder="Enter New Password" class="form-control" required>
</div>

<button type="submit" class="btn btn-primary">update</button>
 
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script>
    let url = window.location.href;
    let path = url.split('/')[4].split('#');
    let period = path[1];
    let expire = "<?= time(); ?>";
    
    if (period < expire) {
        alert('Link has expired');
        location = "<?= FORGOT_PASSWORD; ?>";
    }

    $('form').submit((e) => {
        e.preventDefault();
        
        let email = path[0];
        let password = $('#password').val()

        $.ajax({
            type: 'POST',
            url: "<?= RESET_PASSWORD; ?>",
            data: {
                email: email,
                password: password
            }
        }).done(result => {
            if (result === 'success') {
                alert('Password Updated');
                return location = "<?= LOGIN; ?>";
            } else {
                return alert('An Error Occurred, please try again later');
            }
        })
    });
</script>