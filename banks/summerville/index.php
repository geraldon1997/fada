<?php
session_start();

use App\Core\Application;
use App\Routes\Web;

require_once 'vendor/autoload.php';

Web::register();

define("APP_NAME", "Summerville Credit Union");
define("APP_URL", "http://summerville.test/");
define("ASSETS", APP_URL."assets/");
define("DB_NAME", "banks_summerville");


define("HOME", "/home");
define("ABOUT", "/about");
define("SERVICES", "/services");
define("CONTACT", "/contact");
define("REGISTER", "/register");
define("LOGIN", "/login");
define("FORGOT_PASSWORD", "/forgotpassword");
define("SEND_PASSWORD_RESET_LINK", "/sendpasswordresetlink");
define("RESET_PASSWORD", "/resetpassword");

define("DASHBOARD", '/dashboard');
define("TRANSFER", "/transfer");
define("HISTORY", "/history");
define("PROFILE", "/profile");
define("SETTINGS", "/settings");
define("VERIFY_TOKEN", "/verifytoken");
define("CREATE_PROFILE", "/createprofile");
define("UPDATE_PROFILE", "/updateprofile");
define("CHECK_TRANSFER_PIN", "/checktransferpin");
define("UPDATE_PASSWORD", "/updatepassword");
define("UPDATE_PIN", "/updatepin");

define("LOGOUT", "/logout");

/************************************** admin  **********************************************/

define("VIEW_USERS", "/users");
define("VIEW_USER", "/user");
define("BAN_USER", "/banuser");
define("UNBAN_USER", "/unbanuser");
define("LOCK_ACCOUNT", "/lockaccount");
define("UNLOCK_ACCOUNT", "/unlockaccount");
define("DELETE_USER", "/deleteuser");
define("CREDIT_USER", "/credituser");

if (isset($_SERVER['HTTP_REFERER'])) {
    $http_referer = $_SERVER['HTTP_REFERER'];
} else {
    $http_referer = '/home';
}
define("GO_BACK", $http_referer);

Application::start();
// echo password_hash(123, PASSWORD_BCRYPT);
